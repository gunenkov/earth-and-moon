﻿#define _CRT_SECURE_NO_WARNINGS
#pragma comment(lib, "soil.lib")
#include "SOIL.h"
#include <GL/glut.h>
#include <cmath>
#include <string>
#define WINDOW_WIDTH 1000
#define WINDOW_HEIGHT 1000
#define FPS 144
#define LATENCY 1000/FPS
#define M_PI acos(-1.0)
using namespace std;

/*
	angle0 - rotation of the "Earth-Moon" system in XOZ;
	angle1 - rotation of the LIGHT0;
	angle2 - rotation of the Earth around itself;
	angle3 - rotation of the Moon around itself;
	angle4 - position of the Moon relative to the Earth;
	angle5 - rotation of the "Earth-Moon" system in YOZ;
	angle6 - rotation of the "Earth-Moon" system in XOZ;
	angle7 - rotation of the "Earth-Moon" system in XOY;
*/

double angle0 = 0.0, angle0Increment = 0.4, 
angle0IncrementTemp = 0.0, angle1 = -M_PI, angle2 = 0.0, 
angle3 = 0.0, angle4 = 0.0, angle5 = 0.0,
angle6 = 0.0, angle7 = 0.0, orbitRadius = 860.0;
bool rotation = true, showOrbit = false;
GLuint textures[3];
GLfloat ambientColor[] = { 0.15, 0.17, 0.2, 0.0 },
color0[] = { 1.0, 1.0, 1.0, 0.0 };

void drawEarth();
void drawMoon();
void drawBackGround();
void myDisplay();
void keyboardFunc(unsigned char key, int x, int y);
void timerFunc(int i);
GLuint loadTexture(const char* filename);
void setTexture(GLuint currentTexture);
void setQuadric(GLUquadricObj* quadric);
void setLightModel();
void saveScreenshot();

int main(int argc, char** argv)
{
	glutInit(&argc, argv);
	glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB | GLUT_DEPTH);
	glutInitWindowSize(WINDOW_WIDTH, WINDOW_HEIGHT);
	glutInitWindowPosition(450, 5);
	glutCreateWindow("Earth and Moon");
	glClearColor(0.0f, 0.03f, 0.08f, 0.0f);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	glOrtho(-WINDOW_WIDTH, WINDOW_WIDTH, 
			-WINDOW_HEIGHT, WINDOW_HEIGHT,
			-1000.0, 1000.0);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glutKeyboardFunc(keyboardFunc);
	glutTimerFunc(LATENCY, timerFunc, 1);
	glutDisplayFunc(myDisplay);
	textures[0] = loadTexture("textures/earth.bmp");
	textures[1] = loadTexture("textures/moon.bmp");
	textures[2] = loadTexture("textures/background.bmp");
	glPointSize(3.0);
	glColor3f(1.0, 1.0, 1.0);
	glEnable(GL_SMOOTH);
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_LIGHTING);
	glEnable(GL_LIGHT0);
	glEnable(GL_TEXTURE_2D);
	glutMainLoop();
	glDisable(GL_TEXTURE_2D);
	glDisable(GL_LIGHT0);
	glDisable(GL_LIGHTING);
	glDisable(GL_DEPTH_TEST);
	glDisable(GL_SMOOTH);
}

void myDisplay()
{
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glRotatef(angle5, 1.0f, 0.0f, 0.0f);
	glRotatef(angle6, 0.0f, 1.0f, 0.0f);
	glRotatef(angle7, 0.0f, 0.0f, 1.0f);
	if (showOrbit)
	{
		glDisable(GL_LIGHTING);
		glBegin(GL_POINTS);
		for (double i = 0; i < 2 * M_PI; i += 0.001)
		{
			glVertex3d(orbitRadius * cos(i), 0.0, orbitRadius * sin(i));
			glVertex3d(0.0, orbitRadius * cos(i), orbitRadius * sin(i));
			glVertex3d(orbitRadius * cos(i), orbitRadius * sin(i), 0.0);
		}
		glEnd();
		glEnable(GL_LIGHTING);
	}
	glLoadIdentity();
	setLightModel();
	glLoadIdentity();
	drawBackGround();
	drawEarth();
	drawMoon();
	glutSwapBuffers();
}

void keyboardFunc(unsigned char key, int x, int y)
{
	switch (key)
	{
	case '-': {
		if (rotation)
			if (angle0Increment > 0.3) angle0Increment -= 0.2;
		break;
	}
	case '=': {
		if (rotation)
			if (angle0Increment < 2.0) angle0Increment += 0.2;
		break;
	}
	case 'r': angle1 += M_PI / 32; break;
	case 't': angle1 -= M_PI / 32; break;
	case 32: {
		if (!angle0IncrementTemp)
		{
			angle0IncrementTemp = angle0Increment;
			angle0Increment = 0.0;
			rotation = false;
		}
		else {
			angle0Increment = angle0IncrementTemp;
			angle0IncrementTemp = 0.0;
			rotation = true;
		}
		break;
	}
	case 'l': {
		angle1 = -M_PI;
		angle0 = angle2 = angle3 = 
		angle4 = angle5 = angle6 = 
		angle7 = 0.0;
		break;
	}
	case 'w': angle5 += 3.0; break;
	case 's': angle5 -= 3.0; break;
	case 'a': angle6 += 3.0; break;
	case 'd': angle6 -= 3.0; break;
	case 'f': angle7 += 3.0; break;
	case 'g': angle7 -= 3.0; break;
	case 'q': angle2 -= 1.0; break;
	case 'e': angle2 += 1.5; break;
	case 'z': angle3 -= 1.5; break;
	case 'c': angle3 += 1.0; break;
	case 'v': angle4 -= 2.5; break;
	case 'b': angle4 += 2.5; break;
	case 'x': showOrbit = !showOrbit; break;
	case 'p' : saveScreenshot();
	}
}

void drawEarth()
{
	GLUquadricObj* earth = gluNewQuadric();
	setQuadric(earth);
	setTexture(textures[0]);
	glRotatef(angle5, 1.0f, 0.0f, 0.0f);
	glRotatef(angle6, 0.0f, 1.0f, 0.0f);
	glRotatef(angle7, 0.0f, 0.0f, 1.0f);
	glRotatef(250, 1.0f, 1.0f, 1.0f);
	glRotatef(angle0, 0.0f, 0.0f, 1.0f);
	glRotatef(angle2, 0.0f, 0.0f, 1.0f);
	glMaterialfv(GL_FRONT_AND_BACK, GL_SPECULAR, color0);
	glMaterialf(GL_FRONT_AND_BACK, GL_SHININESS, 14.0f);
	gluSphere(earth, 390.0, 1024, 1024);
	glLoadIdentity();
	gluDeleteQuadric(earth);
}

void drawMoon()
{
	GLUquadricObj* moon = gluNewQuadric();
	setQuadric(moon);
	setTexture(textures[1]);
	glRotatef(angle5, 1.0f, 0.0f, 0.0f);
	glRotatef(angle6, 0.0f, 1.0f, 0.0f);
	glRotatef(angle7, 0.0f, 0.0f, 1.0f);
	glTranslatef(orbitRadius * cos(((angle0 + angle4) * M_PI / 180)),
		0.0f, -orbitRadius * sin((angle0 + angle4) * M_PI / 180));
	glRotatef(250, 1.0f, 1.0f, 1.0f);
	glRotatef(angle0, 0.0f, 0.0f, 1.0f);
	glRotatef(angle3, 0.0f, 0.0f, 1.0f);
	glMaterialfv(GL_FRONT_AND_BACK, GL_SPECULAR, color0);
	glMaterialf(GL_FRONT_AND_BACK, GL_SHININESS, 30.0f);
	gluSphere(moon, 108.0, 1024, 1024);
	glLoadIdentity();
	gluDeleteQuadric(moon);
}

void drawBackGround()
{
	setTexture(textures[2]);
	glBegin(GL_QUADS);
	glTexCoord2f(0.0f, 0.0f); glVertex3i(-1000, -1000, -999);
	glTexCoord2f(0.0f, 1.0f); glVertex3i(-1000, 1000, -999);
	glTexCoord2f(1.0f, 1.0f); glVertex3i(1000, 1000, -999);
	glTexCoord2f(1.0f, 0.0f); glVertex3i(1000, -1000, -999);
	glEnd();
}

void timerFunc(int i)
{
	angle0 += angle0Increment;
	glutPostRedisplay();
	glutTimerFunc(LATENCY, timerFunc, 1);
}

GLuint loadTexture(const char* filename)
{
	GLuint texture = SOIL_load_OGL_texture(filename, SOIL_LOAD_RGB,
		SOIL_CREATE_NEW_ID, SOIL_FLAG_INVERT_Y);
	return texture;
}

void setTexture(GLuint currentTexture)
{
	glBindTexture(GL_TEXTURE_2D, currentTexture);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
}

void setQuadric(GLUquadricObj * quadric)
{
	gluQuadricDrawStyle(quadric, GLU_FILL);
	gluQuadricTexture(quadric, GL_TRUE);
	gluQuadricNormals(quadric, GLU_SMOOTH);
}

void setLightModel()
{
	glLightModelfv(GL_LIGHT_MODEL_AMBIENT, ambientColor);
	GLfloat pos0[] = { cos(angle1), 0.0f, sin(angle1), 0.0f };
	glLightfv(GL_LIGHT0, GL_DIFFUSE, color0);
	glLightfv(GL_LIGHT0, GL_AMBIENT, color0);
	glLightfv(GL_LIGHT0, GL_SPECULAR, color0);
	glLightfv(GL_LIGHT0, GL_POSITION, pos0);
	glLightf(GL_LIGHT0, GL_QUADRATIC_ATTENUATION, 0.05f);
	glLightf(GL_LIGHT0, GL_LINEAR_ATTENUATION, 0.5f);
}

void saveScreenshot()
{
	static char currentScreenshotNumber = 1;
	char* temp = new char[5];
	_itoa(currentScreenshotNumber, temp, 10);
	string fileName = (string)temp + ".bmp";
	SOIL_save_screenshot
	(
		fileName.c_str(),
		SOIL_SAVE_TYPE_BMP,
		0, 0, WINDOW_WIDTH, WINDOW_HEIGHT
	);
	currentScreenshotNumber++;
}
